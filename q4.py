import pickle, gzip, urllib.request, json
import numpy as np
import matplotlib.pyplot as plt

obtain_dataset = True

if obtain_dataset is True:
    urllib.request.urlretrieve("http://deeplearning.net/data/mnist/mnist.pkl.gz", "mnist.pkl.gz")

class DataHandler(object):
    def __init__(self, filename):
        with gzip.open(filename,'rb') as f:
            self.train_set, self.valid_set, self.test_set = pickle.load(f, encoding='latin1')

        self.preprocess()

    def preprocess(self):
        print("Preprocessing")
        mean_train_vector = np.mean(self.train_set[0], 0)

        self.train_set = list(self.train_set)
        self.train_set[0] = np.subtract(self.train_set[0], mean_train_vector)
        self.train_set[1][np.remainder(self.train_set[1], 2) == 1] = -1
        self.train_set[1][np.remainder(self.train_set[1], 2) == 0] = 1
        self.train_set = tuple(self.train_set)

        self.valid_set = list(self.valid_set)
        self.valid_set[0] = np.subtract(self.valid_set[0], mean_train_vector)
        self.valid_set[1][np.remainder(self.valid_set[1], 2) == 1] = -1
        self.valid_set[1][np.remainder(self.valid_set[1], 2) == 0] = 1
        self.valid_set = tuple(self.valid_set)

        self.test_set = list(self.test_set)
        self.test_set[0] = np.subtract(self.test_set[0], mean_train_vector)
        self.test_set[1][np.remainder(self.test_set[1], 2) == 1] = -1
        self.test_set[1][np.remainder(self.test_set[1], 2) == 0] = 1
        self.test_set = tuple(self.test_set)

    def get_analytical_linear_regressor(self, X_train, y_train, lamda):
        m,n = X_train.shape
        M = (X_train.T.dot(X_train)+ 2*lamda*m*np.identity(n))
        w_analytical = np.linalg.pinv(M).dot(X_train.T.dot(y_train))
        b_analytical = (1/m)*(np.ones(m).T.dot(y_train))
        return w_analytical, b_analytical

    def get_gradient_descent_linear_regressor(self, X_train, y_train, learning_rate, steps, lamda, plot_loss=False):
        m, n = X_train.shape

        eta = learning_rate
        T = steps

        # Initialization
        w = np.zeros(n)
        b = 0
        zero_one_loss = []
        squared_loss = []

        for i in range(T):
            M = (X_train.T.dot(X_train) + 2 * lamda * m * np.identity(n))
            dw = (1/m)*(M.dot(w))-(1/m)*(X_train.T.dot(y_train))
            db = b-(1/m)*(np.ones(m).T.dot(y_train))
            w = w - eta * dw
            b = b - eta * db
            if plot_loss is True:
                zero_one_loss.append(self.compute_zero_one_loss(w,b, X_train, y_train))
                squared_loss.append(self.compute_squared_loss(w,b, X_train, y_train))
        if plot_loss is True:
            self.plot_data(range(T), zero_one_loss, squared_loss)

        return w,b

    def compute_zero_one_loss(self, w, b, sample_set, classification_set):
        X=sample_set
        y=classification_set
        m,n = np.shape(X)

        y_hat = np.sign(X.dot(w)+b*np.ones(m))
        Indicator_list = [1 if yh != yv else 0 for (yh,yv) in zip(y_hat, y)]
        zero_one_loss = (1 / m) * np.sum(Indicator_list)
        return zero_one_loss

    def compute_squared_loss(self, w, b, sample_set, classification_set):
        X = sample_set
        y = classification_set
        m, n = np.shape(X)
        square_loss = (1/m)*np.square(np.linalg.norm(X.dot(w)+b*np.ones(m)-y))
        return square_loss

    def plot_data(self, X, Y, Y2=None):
        plt.figure()
        plt.scatter(X, Y)
        if Y2 is not None:
            plt.plot(X, Y2, 'r-')
        plt.title("Loss vs Step")
        plt.xlabel('step')
        plt.ylabel('loss')
        plt.show()

if __name__ == "__main__":
    # A: Preprocessing is done in init function
    dh = DataHandler("mnist.pkl.gz")
    # B:
    lamda_vec = np.logspace(-5, 2, 8)
    # w_b_analytic_vs_lamda = [dh.get_analytical_linear_regressor(dh.train_set[0], dh.train_set[1], lamda) for lamda in lamda_vec]
    zero_one_loss_analytical_training = []
    zero_one_loss_gd_training=[]
    zero_one_loss_analytical_validation=[]
    zero_one_loss_gd_validation=[]
    zero_one_loss_analytical_test=[]
    zero_one_loss_gd_test=[]
    squared_loss_analytical_training=[]
    squared_loss_gd_training=[]
    squared_loss_analytical_validation=[]
    squared_loss_gd_validation=[]
    squared_loss_analytical_test=[]
    squared_loss_gd_test=[]

    for i in range(len(lamda_vec)):
        # B
        print("Lamda=%f"%lamda_vec[i])
        print("\tCalculating Analytical solution")
        w_analytical,b_analytical = dh.get_analytical_linear_regressor(dh.train_set[0], dh.train_set[1], lamda_vec[i])
        print("\tCalculating GD Solution")
        w_gd, b_gd = dh.get_gradient_descent_linear_regressor(dh.train_set[0], dh.train_set[1], learning_rate=0.001, steps=100, lamda=lamda_vec[i])

        # C
        print("\tAnalytical Training Scheme losses: ")
        print("\t\tZero-One Losses: ")


        zero_one_loss_analytical_training.append(
            dh.compute_zero_one_loss(w_analytical, b_analytical, dh.train_set[0], dh.train_set[1]))
        zero_one_loss_analytical_validation.append(dh.compute_zero_one_loss(w_analytical, b_analytical, dh.valid_set[0],
                                                                            dh.valid_set[1]))
        zero_one_loss_analytical_test.append(dh.compute_zero_one_loss(w_analytical, b_analytical, dh.test_set[0],
                                                                      dh.test_set[1]))
        print("\t\t\tTraining - %f" % zero_one_loss_analytical_training[i])
        print("\t\t\tValidation - %f" % zero_one_loss_analytical_validation[i])
        print("\t\t\tTest - %f" % zero_one_loss_analytical_test[i])

        print("\t\tSquared Losses: ")

        squared_loss_analytical_training.append(dh.compute_squared_loss(w_analytical, b_analytical, dh.train_set[0],
                                                                        dh.train_set[1]))
        squared_loss_analytical_test.append(dh.compute_squared_loss(w_analytical, b_analytical, dh.test_set[0],
                                                                    dh.test_set[1]))
        squared_loss_analytical_validation.append(dh.compute_squared_loss(w_analytical, b_analytical, dh.valid_set[0],
                                                                          dh.valid_set[1]))
        print("\t\t\tTraining - %f" % squared_loss_analytical_training[i])
        print("\t\t\tValidation - %f" % squared_loss_analytical_validation[i])
        print("\t\t\tTest - %f" % squared_loss_analytical_test[i])

        print("\tGD Training Scheme losses: ")
        print("\t\tZero-One Losses: ")

        zero_one_loss_gd_training.append(dh.compute_zero_one_loss(w_gd, b_gd, dh.train_set[0], dh.train_set[1]))
        zero_one_loss_gd_validation.append(dh.compute_zero_one_loss(w_gd, b_gd, dh.valid_set[0], dh.valid_set[1]))
        zero_one_loss_gd_test.append(dh.compute_zero_one_loss(w_gd, b_gd, dh.test_set[0], dh.test_set[1]))
        print("\t\t\tTraining - %f" % zero_one_loss_gd_training[i])
        print("\t\t\tValidation - %f" % zero_one_loss_gd_validation[i])
        print("\t\t\tTest - %f" % zero_one_loss_gd_test[i])

        print("\t\tSquared Losses: ")

        squared_loss_gd_training.append(dh.compute_squared_loss(w_gd, b_gd, dh.train_set[0], dh.train_set[1]))
        squared_loss_gd_validation.append(dh.compute_squared_loss(w_gd, b_gd, dh.valid_set[0], dh.valid_set[1]))
        squared_loss_gd_test.append(dh.compute_squared_loss(w_gd, b_gd, dh.test_set[0], dh.test_set[1]))
        print("\t\t\tTraining - %f" % squared_loss_gd_training[i])
        print("\t\t\tValidation - %f" % squared_loss_gd_validation[i])
        print("\t\t\tTest - %f" % squared_loss_gd_test[i])


    # D:
    analytic_min_zero_one_loss_validation, min_lamda_index_analytical = min((val, idx) for (idx, val) in enumerate(zero_one_loss_analytical_validation))
    gd_min_zero_one_loss_validation, min_lamda_index_gd = min(
        (val, idx) for (idx, val) in enumerate(zero_one_loss_gd_validation))
    print(
        "Analytic Scheme - The squared loss on the test set of the model which performed best on the validation test in terms of 0-1 loss - %f" %
        squared_loss_analytical_test[min_lamda_index_analytical])
    print(
        "GD Scheme - The squared loss on the test set of the model which performed best on the validation test in terms of 0-1 loss - %f" %
        squared_loss_gd_test[min_lamda_index_gd])

    # E:
    dh.get_gradient_descent_linear_regressor(dh.train_set[0], dh.train_set[1], learning_rate=0.001, steps=100, lamda=lamda_vec[min_lamda_index_gd], plot_loss=True)
    dh.get_gradient_descent_linear_regressor(dh.test_set[0], dh.test_set[1], learning_rate=0.001, steps=100,
                                             lamda=lamda_vec[min_lamda_index_gd], plot_loss=True)

